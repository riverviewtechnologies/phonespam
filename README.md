In this repository, we have phone numbers that we have marked as spam.
If you feel that your phone number has been erroneously added, please visit http://riverviewtechnologies.com/phonespam and follow the instructions there.

Code in this repository is licensed under the GPLv3 or later.
The phone data base is licensed under the CC-BY-NC-SA.